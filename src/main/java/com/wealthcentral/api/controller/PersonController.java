package com.wealthcentral.api.controller;

import com.wealthcentral.api.model.Address;
import com.wealthcentral.api.model.Children;
import com.wealthcentral.api.model.Person;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RestController
public class PersonController {

    /**
     * Endpoint to save Person
     * @param person
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/person", method = RequestMethod.POST)
    public Person savePerson( @RequestBody Person person) throws Exception {
        if (person==null || StringUtils.isEmpty(person.firstname) || StringUtils.isEmpty(person.lastname)){
            //returning a Exception -> the name of the exception returned should be more explicit.
            //We could also create an exception handler in order to return some custom object
            throw new Exception("Missing person Object or Missing the required fields");
        }else{
            return person;
        }
    }

    /**
     * Return a person with most of the fields populated
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/person", method = RequestMethod.GET)
    public Person getPerson() throws ParseException {
        Address address=new Address("33 Redleaf Court", "","Burleigh Waters","QLD","4220");
        List children = Arrays.asList(new Children("Perfect","Forecast"), new Children("NewProject","InProgress"));
        SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy",Locale.ENGLISH);

        String dateInString = "03/01/1984";
        Date dob = formatter.parse(dateInString);
        //the date is not initializing. I didn´t investigate further
        return new Person("Geoffrey","Thomazeau",36,dob,address,children );
    }
}
