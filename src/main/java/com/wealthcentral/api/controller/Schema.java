package com.wealthcentral.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.JsonSchemaGenerator;
import com.wealthcentral.api.model.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/schema")
public class Schema {
    /**
     * Return the Json Schema of the class Person
     * @return
     * @throws JsonProcessingException
     */
    @GetMapping("/person")
    public JsonSchema person() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonSchemaGenerator schemaGen = new JsonSchemaGenerator(mapper);
        JsonSchema schema = schemaGen.generateSchema(Person.class);

        //we could improve the schema by using com.fasterxml.jackson.module.jsonSchema.customProperties.TitleSchemaFactoryWrapper to add some custom properties according the the UI annotation from metawidget
        //this solution would avoid to have to do it in the front end
        //ps: the JsonProperty attribute "required" is already supported
        return schema;
    }

}
