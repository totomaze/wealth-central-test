package com.wealthcentral.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class Person {
    @JsonProperty(required = true)
    public String firstname;
    @JsonProperty(required = true)
    public String lastname;
    public Number age;
    public Date dob;
    public Address address;
    public List<Children> children;

    public Person(String firstname, String lastname, Number age, Date dob, Address address, List<Children> children) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.dob = dob;
        this.address = address;
        this.children = children;
    }
}
