package com.wealthcentral.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Children {
    @JsonProperty(required = true)
    public String firstname;
    @JsonProperty(required = true)
    public String lastname;

    public Children(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
}
