package com.wealthcentral.api.model;

public class Address {
    public String street;
    public String optional;
    public String city;
    public String state;
    public String postalCode;

    public Address(String street, String optional, String city, String state, String postalCode) {
        this.street = street;
        this.optional = optional;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
    }
}
