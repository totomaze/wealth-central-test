(function () {

    'use strict';

    /**
     * Controller
     */

    angular.module('app').controller('simpleController', function ($scope, $http) {

        /**
         * Rest Api Configuration
         */
        const baseUrl = "./";
        const schemaPersonEndpoint = "/schema/person";
        const personEndpoint = "person";

        function getSchemaPerson() {
            return new Promise(function (resolve, reject) {
                $http({
                    method: 'GET'
                    , url: baseUrl + schemaPersonEndpoint
                }).then(function successCallback(response) {
                    console.log(response);
                    resolve(response);
                }, function errorCallback(response) {
                    reject(response);
                });
            });
        }

        function postPerson(data) {
            return new Promise(function (resolve, reject) {
                $http({
                    method: 'POST'
                    , url: baseUrl + personEndpoint
                    , data: data
                }).then(function successCallback(response) {
                    resolve(response.data);
                }, function errorCallback(response) {
                    reject(response.data);
                });
            });
        }

        function getPerson() {
            return new Promise(function (resolve, reject) {
                $http({
                    method: 'GET'
                    , url: baseUrl + personEndpoint
                }).then(function successCallback(response) {
                    resolve(response.data);
                }, function errorCallback(response) {
                    reject(response.data);
                });
            });
        }


        /**
         * schema returned by /schema/person
         * {"type":"object","id":"urn:jsonschema:com:wealthcentral:api:model:Person","properties":{"firstname":{"type":"string","required":true},"lastname":{"type":"string","required":true},"age":{"type":"number"},"dob":{"type":"integer","format":"utc-millisec"},"address":{"type":"object","id":"urn:jsonschema:com:wealthcentral:api:model:Address","properties":{"street":{"type":"string"},"optional":{"type":"string"},"city":{"type":"string"},"state":{"type":"string"},"postalCode":{"type":"string"}}},"children":{"type":"array","items":{"type":"object","id":"urn:jsonschema:com:wealthcentral:api:model:Children","properties":{"firstname":{"type":"string","required":true},"lastname":{"type":"string","required":true}}}}}};
         */

            //I did play with section, required , hidden in order to see the change in the UI (pretty useful),before using the version using bootstrap
            // after I used this example in order to use an array for children, but I could see much change in the UI (I guess it s because of bootstrap),
			// but I can see, some extra fields are translated into ng-attributes
        const restSchema = {
                type: "person",
                properties: {
                    firstname: {type: "string", required: true},
                    lastname: {type: "string", required: "true"},
                    age: {
                        type: "number",
                        min: '0',
                        max: '100',
                        minLength: '0',
                        maxLength: '5',
                        minValue: '0',
                        maxValue: '100'
                    },
                    dob: {
                        hidden: false,
                        label: "Date of birth",
                        type: 'date',
                    }
                    , address: {
                        properties: {
                            street: {type: "string"},
                            optional: {type: "string"},
                            city: {type: "string"},
                            postalCode: {type: "string"}
                        }
                    },
                    children: {
                        type: 'array',
                        items: {
                            properties: {
                                firstname: {
                                    type: 'string'
                                },
                                lastname: {
                                    type: 'string'
                                }
                            }
                        }
                    },
                    edit: {
                        type: 'function',
                        hidden: '{{!readOnly}}'
                    },
                    save: {
                        type: 'function',
                        hidden: '{{readOnly}}'
                    },
                    reset: {
                        type: 'function',
                        hidden: 'false'
                    }
                }
            }

        /**
         * Metawidget config.
         */

        $scope.metawidgetConfig = {

            /**
             * I run into a problem using the property hidden id: {type: "number",hidden:"true"}, => error comming from metawidget-bootstrap.min.js
			 * (Looking at the source of this file would have help)
             */

            inspector:
                new metawidget.inspector.JsonSchemaInspector(restSchema)
            ,
            /**
             * I did try to load the JsonSchema asynchronously with the following but with the nested class, I ran into a loop generated mw.buildWidgets
             * I guess I was miss-using the inspectionResultProcessors
             */
            // inspectionResultProcessors: [function (inspectionResult, mw, toInspect, type, names) {
            //     getSchemaPerson().then(function(response){
            //         metawidget.util.combineInspectionResults(inspectionResult, response.data);
            //         mw.buildWidgets(inspectionResult);
            //     })
            //     // Return nothing to suspend Metawidget operation until REST call returns
            // }],

            /**
             * Custom WidgetBuilder to instantiate our custom directive.
             */

            widgetBuilder: new metawidget.widgetbuilder.CompositeWidgetBuilder([function (elementName, attributes, mw) {

                // Editable tables

                if (attributes.type === 'array' && !metawidget.util.isTrueOrTrueString(attributes.readOnly)) {

                    var typeAndNames = metawidget.util.splitPath(mw.path);

                    if (typeAndNames.names === undefined) {
                        typeAndNames.names = [];
                    }

                    typeAndNames.names.push(attributes.name);
                    typeAndNames.names.push('0');

                    var inspectionResult = mw.inspect(mw.toInspect, typeAndNames.type, typeAndNames.names);
                    var inspectionResultProperties = metawidget.util.getSortedInspectionResultProperties(inspectionResult);
                    var columns = '';

                    for (var loop = 0, length = inspectionResultProperties.length; loop < length; loop++) {

                        var columnAttribute = inspectionResultProperties[loop];

                        if (metawidget.util.isTrueOrTrueString(columnAttribute.hidden)) {
                            continue;
                        }

                        if (columns !== '') {
                            columns += ',';
                        }
                        columns += columnAttribute.name;
                    }

                    var widget = $('<table>').attr('edit-table', '').attr('columns', columns).attr('ng-model', mw.path + '.' + attributes.name);
                    return widget[0];
                }
            }, new metawidget.widgetbuilder.HtmlWidgetBuilder()]),
            addWidgetProcessors: [new metawidget.bootstrap.widgetprocessor.BootstrapWidgetProcessor()],
            layout: new metawidget.bootstrap.layout.BootstrapDivLayout()
        }

        /**
         * Function associated to the Model
         */

        function initPersonFunction(person) {
            person.edit = function () {
                $scope.readOnly = false;
            };
            person.save = function () {
                $scope.readOnly = true;
                postPerson($scope.person).then(function (response) {
                    console.log(response);
                    alert("saved successfully");
                }).catch(function (response) {
                    console.log(response);
                    alert("Something went wrong: " + response.message);
                })
            };
            person.reset = function () {
                initPerson();
            };
            $scope.person = person;
            $scope.$apply();
        }

        function initPerson() {
            getPerson().then(function (response) {
                initPersonFunction(response);
            });
        }

        /**
         * Initiating the Model
         */
        initPerson();

        $scope.readOnly = true;

    });
})();
