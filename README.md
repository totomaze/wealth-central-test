# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Test for WealthCentral

### Build ###

```
mvn clean install
```

### Run ###

```
mvn spring-boot:run
```
by default http://localhost:8080

### Live URL ###
http://54.190.212.55:8888/

### Endpoints Available ###
* GET     /
* GET     /schema/person
* GET     /person
* POST    /person


### Who do I talk to? ###

Geoffrey Thomazeau
